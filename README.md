Terraform VPC
==========

A Terraform stack that contains builds a VPC to be used as your base for other projects. The intention is to be run using Gitlab.

# Instructions

### Environment Variables

You can add the AWS keys to the project Environment variables by going to `YourProject > Settings> CI/CD > Variables`. From here you will want to add the following for this project:
* `TF_VAR_AWS_ACCESS_KEY_ID`
* `TF_VAR_AWS_SECRET_ACCESS_KEY`
* `TERRAFORM_VERSION`

You put `TF_VAR_` on the front of any variables that Terraform will need to pass into the stack. `TERRAFORM_VERSION` is only used in the Gitlab CI file so no `TF_VAR_` needed

### Files to Edit
* `gitab-ci.yml` - Rename this file to `.gitlab-ci.yml`
* `backend-state.tf` - Pretty obvious if you look at it.
* `outputs.tf` - Replace all of the `VPC_NAME` entries with the name of your VPC that you also put in `vpc.tf`
* `variables.tf` - Main setting for REGION is found here.
* `vpc.tf` - Be sure to change the all caps options with comments
* `.gitlab-ci.yml` - I added a DESTROY option at the end of the pipeline. If you plan on using this in any sort of "production" environment where you dont want anyone to accidentally rip the VPC out from under your stuff...delete this section. Dont just comment it out, DELETE it. See the `PipelineIllustration.png` file for an example of what the CI file gives you.


# Forks and Contributing

Feel free to clone this project and use for yourself. This is a very basic example and can be greatly expanded. Just give me a mention if you use it.
Fork if you have something to contribute or open an issue.

Feel free to comment or contact me on Twitter: `@setheryops`
