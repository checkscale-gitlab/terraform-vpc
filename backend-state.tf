terraform {
  backend "s3" {
    bucket = "YOUR_BUCKET_NAME"
    key    = "NAME_YOUR_STATE_FILE.tfstate"
    region = "ENTER_YOUR_REGION"
  }
}
